/**
 * Model for user login to the application
 */
export interface UserLogin {
  nickname: string;
  password: string;
}
