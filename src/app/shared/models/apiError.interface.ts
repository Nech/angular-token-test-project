/**
 * ApiError simplifies the handling of backend server errors
 */
export interface ApiError {
  status: number;
  statusText: string;
  message: string;
}
