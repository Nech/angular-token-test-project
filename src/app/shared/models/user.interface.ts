export interface User {
  id: string;
  nickname: string;
  role: string;
  created_at: number;
  updated_at: number;
}
