/**
 * Application response on user login
 */
export interface TokenPair {
  access_token: string;
  refresh_token: string;
}

/**
 * Application response when requesting to update the token
 */
export interface AuthToken {
  access_token: string;
}

/**
 * Model to update the authorization token
 */
export interface RefreshToken {
  refresh_token: string;
}
