export interface SideMenu {
  text: string;
  link: string;
}
