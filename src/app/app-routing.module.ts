import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '@modules/layout/pages/layout/layout.component';
import { HomePageComponent } from '@modules/home/pages/home-page/home-page.component';
import { canActivateChildGuard } from '@core/guards';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivateChild: [canActivateChildGuard],
    children: [
      {
        path: '',
        component: HomePageComponent,
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('@modules/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'profile',
    component: LayoutComponent,
    canActivateChild: [canActivateChildGuard],
    loadChildren: () =>
      import('@modules/profile/profile.module').then(m => m.ProfileModule),
  },
  {
    path: '404',
    component: LayoutComponent,
    canActivateChild: [canActivateChildGuard],
    loadChildren: () =>
      import('@modules/not-found/not-found.module').then(m => m.NotFoundModule),
  },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
