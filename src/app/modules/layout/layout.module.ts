import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { LayoutComponent } from './pages/layout/layout.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [HeaderComponent, SideMenuComponent, LayoutComponent],
  exports: [LayoutComponent],
  imports: [CommonModule, LayoutRoutingModule, SharedModule],
})
export class LayoutModule {}
