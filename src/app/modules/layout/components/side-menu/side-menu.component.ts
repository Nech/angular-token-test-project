import { Component } from '@angular/core';
import { SideMenu } from '@shared/models';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent {
  sideMenu: SideMenu[] = [
    { text: 'Home', link: '/' },
    { text: 'Profile', link: '/profile' },
  ];

  /**
   * Optimize *ngFor
   * @param index
   * @param item
   */
  trackByMenuItem(index: number, item: SideMenu): string {
    return item.text;
  }
}
