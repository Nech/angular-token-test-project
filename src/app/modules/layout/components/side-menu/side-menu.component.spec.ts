import { SideMenuComponent } from '@modules/layout/components/side-menu/side-menu.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SideMenu } from '@shared/models';

describe('SideMenuComponent', () => {
  let component: SideMenuComponent;
  let fixture: ComponentFixture<SideMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SideMenuComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(SideMenuComponent);
    component = fixture.componentInstance;
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('trackByMenuItem', () => {
    it('should return text of menu', () => {
      const item: SideMenu = {
        text: 'foo',
        link: '/bar',
      };
      const res = component.trackByMenuItem(0, item);

      expect(res).toEqual(item.text);
    });
  });
});
