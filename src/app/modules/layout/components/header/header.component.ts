import { Component, EventEmitter, Output } from '@angular/core';
import { AuthService } from '@core/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  @Output() toggleMenu = new EventEmitter();
  constructor(private authService: AuthService) {}

  /**
   * Toggle side menu on click
   */
  onToggleMenu() {
    this.toggleMenu.emit();
  }

  /**
   * Sign-out the current user, delete tokens and redirect to sign in page
   */
  logout() {
    this.authService.logout();
  }
}
