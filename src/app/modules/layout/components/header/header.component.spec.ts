import { HeaderComponent } from '@modules/layout/components/header/header.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthService } from '@core/services';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [{ provide: AuthService, useValue: { logout() {} } }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('onToggleMenu', () => {
    it('should emit toggleMenu event', () => {
      const spy = spyOn(component.toggleMenu, 'emit');
      component.onToggleMenu();

      expect(spy).toHaveBeenCalled();
    });
  });

  describe('logout', () => {
    it('should call the service method', () => {
      const spy = spyOn(authService, 'logout');
      component.logout();

      expect(spy).toHaveBeenCalled();
    });
  });
});
