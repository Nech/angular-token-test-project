import { AuthPageComponent } from '@modules/auth/pages/auth-page/auth-page.component';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { AuthService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { BehaviorSubject, of, throwError } from 'rxjs';
import { ApiError, AuthToken, TokenPair, UserLogin } from '@shared/models';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

const tokenSubject = new BehaviorSubject<AuthToken | null>(null);
const AuthServiceStub = {
  get tokenValue() {
    return tokenSubject.value;
  },
  login: () => {},
};

let snapshotParam = {
  queryParams: { returnUrl: 'test-1' },
};
const routeMock: any = {
  snapshot: snapshotParam,
};
const routes: Routes = [
  { path: 'test', component: AuthPageComponent },
  { path: 'test-1', component: AuthPageComponent },
];

describe('AuthPageComponent', () => {
  let component: AuthPageComponent;
  let fixture: ComponentFixture<AuthPageComponent>;
  let authService: AuthService, toasterService: ToastrService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AuthPageComponent],
      providers: [
        FormBuilder,
        { provide: ActivatedRoute, useValue: routeMock },
        { provide: AuthService, useValue: AuthServiceStub },
        { provide: ToastrService, useValue: { error() {} } },
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(routes),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(AuthPageComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    toasterService = TestBed.inject(ToastrService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('testing redirect to the home page on load', () => {
    beforeEach(() => router.navigate(['test']));

    it('should not redirect to home page if access token is null', fakeAsync(() => {
      tokenSubject.next(null);
      TestBed.createComponent(AuthPageComponent);
      tick();

      expect(router.url).toBe('/test'); // not changed
    }));

    it('should redirect to home page if access token defined', fakeAsync(() => {
      const token: AuthToken = { access_token: 'foo' };
      tokenSubject.next(token);
      TestBed.createComponent(AuthPageComponent);
      tick();

      expect(router.url).toBe('/'); // changed
    }));
  });

  describe('test login form', () => {
    it('should create form with 2 controls', () => {
      expect(component.form.contains('nickname')).toBeTruthy();
      expect(component.form.contains('password')).toBeTruthy();
    });

    it('should mark controls as invalid if value is empty', () => {
      let control = component.form.get('nickname');
      control?.setValue('');
      expect(control?.valid).toBeFalse();

      control = component.form.get('password');
      control?.setValue('');
      expect(control?.valid).toBeFalse();
    });

    it('should mark controls as valid if value is not empty', () => {
      let control = component.form.get('nickname');
      control?.setValue('foo');
      expect(control?.valid).toBeTruthy();

      control = component.form.get('password');
      control?.setValue('bar');
      expect(control?.valid).toBeTruthy();
    });
  });

  describe('getFieldError testing', () => {
    const fieldName = 'nickname';
    let control: AbstractControl<any, any> | null;
    beforeEach(() => (control = component.form.get(fieldName)));

    it('should return message if field is invalid', () => {
      control?.setValue('');

      expect(component.form.get(fieldName)?.hasError('required')).toBeTruthy();
      expect(!!component.getFieldError(fieldName)).toBeTruthy();
    });

    it('should return empty string if field is valid', () => {
      control?.setValue('foo');

      expect(component.form.get(fieldName)?.hasError('required')).toBeFalse();
      expect(!!component.getFieldError(fieldName)).toBeFalse();
    });
  });

  describe('convertUserName testing', () => {
    it('should convert nickname to lower case', () => {
      const control = component.form.get('nickname');
      control?.setValue(' FoO ');
      component.convertUserName();

      expect(control?.value).toBe('foo');
    });
  });

  describe('onSubmit testing', () => {
    const loginData: UserLogin = {
      nickname: 'foo',
      password: 'bar',
    };
    const expectedTokens: TokenPair = {
      access_token: 'access',
      refresh_token: 'refresh',
    };
    const errorResponse: ApiError = {
      status: 400,
      statusText: 'foo',
      message: 'bar',
    };
    beforeEach(() => router.navigate(['test']));

    it('should not send data if form is invalid', () => {
      const spy = spyOn(authService, 'login');
      expect(component.form.invalid).toBeTruthy();
      component.onSubmit();

      expect(spy).not.toHaveBeenCalled();
    });

    it('should sent data successfully and redirect to returnUrl', fakeAsync(() => {
      component.form.get('nickname')?.setValue('foo');
      component.form.get('password')?.setValue('bar');
      expect(component.form.invalid).toBeFalse();
      const spy = spyOn(authService, 'login').and.returnValue(
        of(expectedTokens)
      );

      component.onSubmit();
      tick();

      expect(spy).toHaveBeenCalledWith(loginData);
      expect(router.url).toBe(`/${snapshotParam.queryParams.returnUrl}`); // url changed
    }));

    it('should not redirect and show error', fakeAsync(() => {
      component.form.get('nickname')?.setValue('foo');
      component.form.get('password')?.setValue('bar');
      expect(component.form.invalid).toBeFalse();

      const spy = spyOn(authService, 'login').and.returnValue(
        throwError(() => errorResponse)
      );
      const spyHandler = spyOn(toasterService, 'error');

      component.onSubmit();
      tick();

      expect(spy).toHaveBeenCalledWith(loginData);
      expect(router.url).toBe('/test'); // url not changed
      expect(spyHandler).toHaveBeenCalledWith(errorResponse.message);
    }));
  });
});
