import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@core/services';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiError, UserLogin } from '@shared/models';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.scss'],
})
export class AuthPageComponent {
  form: FormGroup;
  disabled = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService
  ) {
    // redirect to home page  if already logged in
    if (this.authService.tokenValue) {
      this.router.navigate(['/']).then();
    }
    // init form
    this.form = formBuilder.group({
      nickname: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  /**
   * Check form validation errors
   * @param fieldName
   */
  public getFieldError(fieldName: string): string {
    if (this.form.get(fieldName)?.hasError('required')) {
      return 'You must enter a value';
    }

    return '';
  }

  /**
   * Allow username to be entered in lowercase only
   */
  public convertUserName(): void {
    this.form
      .get('nickname')
      ?.setValue(this.form.get('nickname')?.value.toLowerCase().trim());
  }

  /**
   * Get a user token to enter the application
   */
  public onSubmit() {
    if (this.form.invalid) return;

    this.disabled = true;
    const userData: UserLogin = {
      ...this.form.value,
    };

    this.authService
      .login(userData)
      .pipe(take(1))
      .subscribe({
        next: () => {
          // clear form inputs
          this.resetForm();
          // get return url from route parameters or default to '/'
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigate([returnUrl]).then();
        },
        error: (err: ApiError) => {
          this.disabled = false;
          this.handleError(err);
        },
      });
  }

  /**
   * Handle validation error (or other error)
   * @param err
   * @private
   */
  private handleError(err: ApiError) {
    const messages = [];
    if (422 == err.status) {
      // validation error as "Key:Foo, Error:Bar\n Key:Baz, Error:Foo"
      const data = err.message.split('\n');
      for (const el of data) {
        const msg = el.split('Error:');
        messages.push(msg[1]);
      }
    } else {
      // other (default) error
      messages.push(err.message);
    }

    for (const msg of messages) {
      this.toast.error(msg);
    }
  }

  /**
   * Reset the form after submitting
   * @private
   */
  private resetForm(): void {
    this.form.reset();
    this.disabled = false;

    Object.keys(this.form.controls).forEach(key => {
      const control = this.form.controls[key];
      control.setErrors(null);
    });
  }
}
