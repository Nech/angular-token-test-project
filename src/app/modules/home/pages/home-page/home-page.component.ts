import { Component, OnInit } from '@angular/core';
import { ApiError, User } from '@shared/models';
import { UserService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  users: User[] = [];
  displayedColumns: string[] = ['id', 'nickname', 'role'];
  loading = true;

  constructor(private userService: UserService, private toast: ToastrService) {}

  /**
   * Get all users
   */
  ngOnInit(): void {
    this.userService
      .getAll()
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.users = res;
        },
        error: (err: ApiError) => {
          this.toast.error(err.message);
          this.loading = false;
        },
        complete: () => {
          this.loading = false;
        },
      });
  }
}
