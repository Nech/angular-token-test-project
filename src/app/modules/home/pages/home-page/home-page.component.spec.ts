import { HomePageComponent } from '@modules/home/pages/home-page/home-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ApiError, User } from '@shared/models';
import { of, throwError } from 'rxjs';
import { MatTableModule } from '@angular/material/table';

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;
  let userService: UserService, toasterService: ToastrService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomePageComponent],
      providers: [
        UserService,
        { provide: ToastrService, useValue: { error() {} } },
      ],
      imports: [HttpClientModule, MatTableModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    toasterService = TestBed.inject(ToastrService);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('displayedColumns property should be defined', () => {
    expect(component.displayedColumns.length).toBe(3);
  });

  describe('get list of users on load', () => {
    it('success', () => {
      const expected: User[] = [
        {
          id: 'user-id',
          nickname: 'foo',
          role: 'bar',
          created_at: 0,
          updated_at: 0,
        },
      ];
      spyOn(userService, 'getAll').and.returnValue(of(expected));
      fixture.detectChanges();

      expect(component.users).toEqual(expected);
      expect(component.loading).toBeFalse();
    });

    it('error', () => {
      const errorResponse: ApiError = {
        status: 400,
        statusText: 'foo',
        message: 'bar',
      };
      const spy = spyOn(toasterService, 'error');
      spyOn(userService, 'getAll').and.returnValue(
        throwError(() => errorResponse)
      );
      fixture.detectChanges();

      expect(component.users.length).toBe(0);
      expect(component.loading).toBeFalse();
      expect(spy).toHaveBeenCalledWith(errorResponse.message);
    });
  });
});
