import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotFoundPageComponent } from '@modules/not-found/pages/not-found-page/not-found-page.component';
import { By } from '@angular/platform-browser';

describe('NotFoundPageComponent', () => {
  let component: NotFoundPageComponent;
  let fixture: ComponentFixture<NotFoundPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NotFoundPageComponent],
    });
    fixture = TestBed.createComponent(NotFoundPageComponent);
    component = fixture.componentInstance;
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  it('should contain "404" in the title', () => {
    expect(component.title).toContain('404');
  });

  it('should contain "not found" in the subtitle', () => {
    expect(component.subtitle).toContain('not found');
  });
});
