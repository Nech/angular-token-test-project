import { Component, OnInit } from '@angular/core';
import { ApiError, User } from '@shared/models';
import { UserService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit {
  user?: User;
  loading = true;

  constructor(private userService: UserService, private toast: ToastrService) {}

  /**
   * Get current user data
   */
  ngOnInit(): void {
    this.userService
      .getCurrent()
      .pipe(take(1))
      .subscribe({
        next: res => {
          this.user = res;
          this.loading = false;
        },
        error: (err: ApiError) => {
          this.toast.error(err.message);
          this.loading = false;
        },
      });
  }
}
