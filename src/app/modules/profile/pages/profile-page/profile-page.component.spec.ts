import { ProfilePageComponent } from '@modules/profile/pages/profile-page/profile-page.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UserService } from '@core/services';
import { ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ApiError, User } from '@shared/models';
import { of, throwError } from 'rxjs';

describe('ProfilePageComponent', () => {
  let component: ProfilePageComponent;
  let fixture: ComponentFixture<ProfilePageComponent>;
  let userService: UserService, toasterService: ToastrService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProfilePageComponent],
      providers: [
        UserService,
        { provide: ToastrService, useValue: { error() {} } },
      ],
      imports: [HttpClientModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(ProfilePageComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    toasterService = TestBed.inject(ToastrService);
  });

  it('should be defined', () => {
    expect(component).toBeDefined();
  });

  describe('get user data on load', () => {
    it('success', () => {
      const expected: User = {
        id: 'user-id',
        nickname: 'foo',
        role: 'bar',
        created_at: 0,
        updated_at: 0,
      };
      spyOn(userService, 'getCurrent').and.returnValue(of(expected));
      fixture.detectChanges();

      expect(component.user).toEqual(expected);
      expect(component.loading).toBeFalse();
    });

    it('error', () => {
      const errorResponse: ApiError = {
        status: 400,
        statusText: 'foo',
        message: 'bar',
      };
      const spy = spyOn(toasterService, 'error');
      spyOn(userService, 'getCurrent').and.returnValue(
        throwError(() => errorResponse)
      );
      fixture.detectChanges();

      expect(component.user).toBeUndefined();
      expect(component.loading).toBeFalse();
      expect(spy).toHaveBeenCalledWith(errorResponse.message);
    });
  });
});
