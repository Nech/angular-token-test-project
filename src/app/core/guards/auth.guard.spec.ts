import { AuthGuard } from '@core/guards/auth.guard';
import { AuthService } from '@core/services';
import { Router, Routes } from '@angular/router';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { BehaviorSubject } from 'rxjs';
import { AuthToken } from '@shared/models';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthPageComponent } from '@modules/auth/pages/auth-page/auth-page.component';

const tokenSubject = new BehaviorSubject<AuthToken | null>(null);
const AuthServiceStub = {
  get tokenValue() {
    return tokenSubject.value;
  },
};
const routes: Routes = [{ path: 'login', component: AuthPageComponent }];
const LOGIN_URL = '/login';

describe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let authService: AuthService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        Router,
        { provide: AuthService, useValue: AuthServiceStub },
      ],
      imports: [RouterTestingModule.withRoutes(routes)],
    });

    authGuard = TestBed.inject(AuthGuard);
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
  });

  it('should allow access if user is authenticated', () => {
    tokenSubject.next({ access_token: 'access' });
    expect(authService.tokenValue).toBeTruthy();

    let res = authGuard.canActivate(router, authService, '/');
    expect(res).toBeTrue();

    res = authGuard.canActivateChild(router, authService, '/');
    expect(res).toBeTrue();
  });

  it('should not allow access if token is not exist and redirect to login page', fakeAsync(() => {
    tokenSubject.next(null);
    expect(authService.tokenValue).toBeFalsy();

    let res = authGuard.canActivate(router, authService, '/');
    tick();

    expect(res).toBeFalse();
    expect(router.url).toContain('/login');
  }));
});
