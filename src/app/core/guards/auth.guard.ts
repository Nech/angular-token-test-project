import { inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateChildFn,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthService } from '../services';

const LOGIN_URL = '/login';

@Injectable({ providedIn: 'root' })
export class AuthGuard {
  /**
   * The guard on a Route
   * @param router
   * @param authService
   * @param returnUrl
   */
  canActivate(
    router: Router,
    authService: AuthService,
    returnUrl: string
  ): boolean {
    const token = authService.tokenValue;
    if (token) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page with the return url
    returnUrl = '/404' === returnUrl ? '/' : returnUrl;
    router
      .navigate([LOGIN_URL], { queryParams: { returnUrl: returnUrl } })
      .then();
    return false;
  }

  /**
   * The guard on a Route child components
   * @param router
   * @param authService
   * @param returnUrl
   */
  canActivateChild(
    router: Router,
    authService: AuthService,
    returnUrl: string
  ): boolean {
    return this.canActivate(router, authService, returnUrl);
  }
}

/**
 * The function used as a canActivate guard on a Route.
 * @param route
 * @param state
 */
export const canActivateGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  return inject(AuthGuard).canActivate(
    inject(Router),
    inject(AuthService),
    state.url
  );
};

/**
 * The function used as a canActivateChild guard on a Route.
 * @param route
 * @param state
 */
export const canActivateChildGuard: CanActivateChildFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  return inject(AuthGuard).canActivateChild(
    inject(Router),
    inject(AuthService),
    state.url
  );
};
