import { Injectable } from '@angular/core';
import { RefreshToken } from '@shared/models';

const REFRESH_TOKEN_KEY = '__auth-refresh-token';

@Injectable({
  providedIn: 'root',
})
export class TokenStorageService {
  public saveRefreshToken(token: RefreshToken): void {
    localStorage.setItem(REFRESH_TOKEN_KEY, token.refresh_token);
  }

  public getRefreshToken(): string {
    return localStorage.getItem(REFRESH_TOKEN_KEY) ?? '';
  }

  public removeRefreshToken(): void {
    localStorage.removeItem(REFRESH_TOKEN_KEY);
  }
}
