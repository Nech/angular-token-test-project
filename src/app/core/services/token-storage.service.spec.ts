import { TokenStorageService } from '@core/services/token-storage.service';
import { TestBed } from '@angular/core/testing';
import { RefreshToken } from '@shared/models';

const token: RefreshToken = {
  refresh_token: 'foo',
};
const REFRESH_TOKEN_KEY = '__auth-refresh-token';

describe('TokenStorageService', () => {
  let tokenStorageService: TokenStorageService;
  let storage: Storage;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenStorageService],
    });
    tokenStorageService = TestBed.inject(TokenStorageService);
    storage = window.localStorage;
  });

  afterEach(() => {
    storage.clear();
  });

  describe('saveRefreshToken testing', () => {
    it('should', () => {
      const spy = spyOn(storage, 'setItem');
      tokenStorageService.saveRefreshToken(token);

      expect(spy).toHaveBeenCalledWith(REFRESH_TOKEN_KEY, token.refresh_token);
    });
  });

  describe('getRefreshToken', () => {
    it('should return token if exists', () => {
      storage.setItem(REFRESH_TOKEN_KEY, token.refresh_token);
      const res = tokenStorageService.getRefreshToken();

      expect(res).toEqual(token.refresh_token);
    });

    it('should return empty string if token removed', () => {
      const res = tokenStorageService.getRefreshToken();

      expect(res).toEqual('');
    });
  });

  describe('removeRefreshToken testing', () => {
    it('should remove token from storage', () => {
      const spy = spyOn(storage, 'removeItem');
      tokenStorageService.removeRefreshToken();

      expect(spy).toHaveBeenCalled();
    });
  });
});
