import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { UserService } from '@core/services/user.service';
import { TestBed } from '@angular/core/testing';
import { User } from '@shared/models';
import { environment } from '@env/environment';

const expectedUserData: User = {
  id: 'user-id',
  nickname: 'foo',
  role: 'bar',
  created_at: 0,
  updated_at: 0,
};

describe('UserService', () => {
  let userService: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService],
    });
    userService = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('getAll testing', () => {
    it('should return list of users', () => {
      const mockData: User[] = [expectedUserData];

      userService.getAll().subscribe(res => {
        expect(res).toEqual(mockData);
      });
      const req = httpMock.expectOne(`${environment.apiUrl}/user`);
      expect(req.request.method).toEqual('GET');
      req.flush(mockData);
    });
  });

  describe('getCurrent testing', () => {
    it('should return user data', () => {
      const mockData: User = expectedUserData;

      userService.getCurrent().subscribe(res => {
        expect(res).toEqual(mockData);
      });
      const req = httpMock.expectOne(`${environment.apiUrl}/user/me`);
      expect(req.request.method).toEqual('GET');
      req.flush(mockData);
    });
  });
});
