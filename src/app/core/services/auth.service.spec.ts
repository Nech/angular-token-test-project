import { AuthService } from '@core/services/auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TokenStorageService } from '@core/services/token-storage.service';
import { Router, Routes } from '@angular/router';
import { AuthToken, TokenPair, UserLogin } from '@shared/models';
import { environment } from '@env/environment';
import { AuthPageComponent } from '@modules/auth/pages/auth-page/auth-page.component';

const tokenPair: TokenPair = {
  access_token:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODE5NzE4NTV9.dvzMhK_O5CJ9ZWau_FczSx0Tm410z1ughDO5dBfdQuQ',
  refresh_token: 'refresh',
};
const loginData: UserLogin = {
  nickname: 'foo',
  password: 'bar',
};
const routes: Routes = [{ path: 'login', component: AuthPageComponent }];

describe('AuthService', () => {
  let authService: AuthService;
  let tokenStorageService: TokenStorageService;
  let httpMock: HttpTestingController;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [AuthService, TokenStorageService],
    });
    httpMock = TestBed.inject(HttpTestingController);
    tokenStorageService = TestBed.inject(TokenStorageService);
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
  });

  describe('tokenValue testing', () => {
    it('should be null by default', () => {
      expect(authService.tokenValue).toBeNull();
    });
  });

  describe('login testing', () => {
    it('should get tokens successfully', fakeAsync(() => {
      const mockData: TokenPair = tokenPair;
      const spy = spyOn(tokenStorageService, 'saveRefreshToken');

      authService.login(loginData).subscribe(res => {
        expect(res).toEqual(mockData);
      });
      const req = httpMock.expectOne(`${environment.apiUrl}/auth/sign-in`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(loginData);
      req.flush(mockData);
      tick();

      // we saved refresh token to localStorage
      expect(spy).toHaveBeenCalledWith({
        refresh_token: mockData.refresh_token,
      });
      // we stored access token in the service
      expect(authService.tokenValue).toEqual({
        access_token: mockData.access_token,
      });
    }));
  });

  describe('refreshToken testing', () => {
    it('should get new access token successfully', fakeAsync(() => {
      const mockData: AuthToken = { access_token: tokenPair.access_token };
      const spy = spyOn(tokenStorageService, 'getRefreshToken').and.returnValue(
        tokenPair.refresh_token
      );

      authService.refreshToken().subscribe(res => {
        expect(res).toEqual(mockData);
      });
      const req = httpMock.expectOne(`${environment.apiUrl}/auth/refresh`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual({
        refresh_token: tokenPair.refresh_token,
      });
      req.flush(mockData);
      tick();

      // we received refresh token from localStorage
      expect(spy).toHaveBeenCalled();
      // we stored access token in the service
      expect(authService.tokenValue).toEqual({
        access_token: mockData.access_token,
      });
    }));
  });

  describe('logout testing', () => {
    it('should remove tokens and redirect to login page', fakeAsync(() => {
      const spy = spyOn(tokenStorageService, 'removeRefreshToken');
      authService.logout();

      const req = httpMock.expectOne(`${environment.apiUrl}/auth/sign-out`);
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toBeNull();
      req.flush(null);
      tick();

      // we removed refresh token from localStorage
      expect(spy).toHaveBeenCalled();
      // we removed access token from the service
      expect(authService.tokenValue).toBeNull();
      // we redirected to the login page
      expect(router.url).toBe('/login');
    }));
  });
});
