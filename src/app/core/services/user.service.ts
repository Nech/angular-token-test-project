import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { User } from '@shared/models';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  /**
   * Get all users
   */
  public getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/user`);
  }

  /**
   * Get user by access token
   */
  public getCurrent() {
    return this.http.get<User>(`${environment.apiUrl}/user/me`);
  }
}
