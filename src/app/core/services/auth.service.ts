import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserLogin, TokenPair, AuthToken, RefreshToken } from '@shared/models';
import { environment } from '@env/environment';
import { map, take } from 'rxjs/operators';
import { TokenStorageService } from './token-storage.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

const LOGIN_URL = '/login';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private tokenSubject: BehaviorSubject<AuthToken | null>;
  private refreshTokenTimeout?: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private tokenStorageService: TokenStorageService
  ) {
    this.tokenSubject = new BehaviorSubject<AuthToken | null>(null);
  }

  /**
   * Return access token value
   */
  public get tokenValue() {
    return this.tokenSubject.value;
  }

  /**
   * Login user to the system and get tokens
   * @param userLogin
   */
  public login(userLogin: UserLogin) {
    return this.http
      .post<TokenPair>(`${environment.apiUrl}/auth/sign-in`, userLogin)
      .pipe(
        map(res => {
          const token: AuthToken = { access_token: res.access_token };
          this.tokenSubject.next(token);
          this.tokenStorageService.saveRefreshToken({
            refresh_token: res.refresh_token,
          });
          this.startRefreshTokenTimer();
          return res;
        })
      );
  }

  /**
   * Update access token
   */
  public refreshToken() {
    const refreshToken: RefreshToken = {
      refresh_token: this.tokenStorageService.getRefreshToken(),
    };
    return this.http
      .post<AuthToken>(`${environment.apiUrl}/auth/refresh`, refreshToken)
      .pipe(
        map(res => {
          this.tokenSubject.next(res);
          this.startRefreshTokenTimer();
          return res;
        })
      );
  }

  /**
   * Request to delete tokens
   */
  public logout() {
    this.http
      .post<any>(`${environment.apiUrl}/auth/sign-out`, null)
      .pipe(take(1))
      .subscribe();

    this.stopRefreshTokenTimer();
    this.tokenSubject.next(null);
    this.tokenStorageService.removeRefreshToken();
    this.router.navigate([LOGIN_URL]).then();
  }

  /**
   * We will refresh the JWT token in the background (silent refresh)
   * one minute before it expires so the user stays logged in
   * @private
   */
  private startRefreshTokenTimer(): void {
    // get access token
    const tokenValue = this.tokenValue;
    const token = tokenValue?.access_token ?? '';

    // parse json object from base64 encoded jwt token
    const tokenPayload = token.split('.')[1];
    const decodedPayload = atob(tokenPayload);
    const parsedPayload = JSON.parse(decodedPayload);

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(parsedPayload.exp * 1000);
    const timeout = expires.getTime() - Date.now() - 60 * 1000;

    // subscribe and update token
    this.refreshTokenTimeout = setTimeout(
      () => this.refreshToken().subscribe(),
      timeout
    );
  }

  /**
   * Stop the timer when the user exits the application
   * @private
   */
  private stopRefreshTokenTimer(): void {
    clearTimeout(this.refreshTokenTimeout);
  }
}
