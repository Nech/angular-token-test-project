import { AuthService, TokenStorageService } from '../services';
import { catchError, Observable, of } from 'rxjs';

/**
 * The app initializer attempts to automatically sign in to the Angular app on startup
 * @param authService
 * @param tokenStorageService
 */
export function appInitializer(
  authService: AuthService,
  tokenStorageService: TokenStorageService
): () => Observable<any> {
  // get refresh token from local storage
  const token = tokenStorageService.getRefreshToken();
  if (!token) {
    // If there is no token, we will not execute the request
    return () => of([]);
  }

  return () =>
    // get a new access token
    authService.refreshToken().pipe(
      // catch error to start app on success or failure
      catchError(() => of([]))
    );
}
