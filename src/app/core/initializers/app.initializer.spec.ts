import { AuthService, TokenStorageService } from '@core/services';
import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { appInitializer } from '@core/initializers/app.initializer';
import { of } from 'rxjs';
import { AuthToken } from '@shared/models';

describe('appInitializer', () => {
  let authServiceSpy: jasmine.SpyObj<AuthService>;
  let tokenStorageServiceSpy: jasmine.SpyObj<TokenStorageService>;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['refreshToken']);
    tokenStorageServiceSpy = jasmine.createSpyObj('TokenStorageService', [
      'getRefreshToken',
    ]);

    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: TokenStorageService, useValue: tokenStorageServiceSpy },
      ],
    });
  });

  it('should return empty array if there is no refresh token', fakeAsync(() => {
    tokenStorageServiceSpy.getRefreshToken.and.returnValue('');
    const result$ = appInitializer(authServiceSpy, tokenStorageServiceSpy)();

    result$.subscribe(result => {
      expect(result).toEqual([]);
    });
    tick();

    expect(tokenStorageServiceSpy.getRefreshToken).toHaveBeenCalled();
    expect(authServiceSpy.refreshToken).not.toHaveBeenCalled();
  }));

  it('should return new access token if refresh token exists', fakeAsync(() => {
    const token: AuthToken = { access_token: 'access' };
    authServiceSpy.refreshToken.and.returnValue(of(token));
    tokenStorageServiceSpy.getRefreshToken.and.returnValue('refresh');
    const result$ = appInitializer(authServiceSpy, tokenStorageServiceSpy)();

    result$.subscribe(result => {
      expect(result).toEqual(token);
    });
    tick();

    expect(tokenStorageServiceSpy.getRefreshToken).toHaveBeenCalled();
    expect(authServiceSpy.refreshToken).toHaveBeenCalled();
  }));
});
