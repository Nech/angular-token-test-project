import {
  HTTP_INTERCEPTORS,
  HttpClient,
  HttpErrorResponse,
} from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ErrorInterceptor } from '@core/interceptors/error.interceptor';
import { AuthService } from '@core/services';
import { BehaviorSubject } from 'rxjs';
import { AuthToken } from '@shared/models';

const url = 'https://test.com';
const tokenSubject = new BehaviorSubject<AuthToken | null>(null);
const AuthServiceStub = {
  get tokenValue() {
    return tokenSubject.value;
  },
  logout: () => {},
};

describe('ErrorInterceptor', () => {
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ErrorInterceptor,
          multi: true,
        },
        { provide: AuthService, useValue: AuthServiceStub },
      ],
      imports: [HttpClientTestingModule],
    });

    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    authService = TestBed.inject(AuthService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should intercept error if status is not 401 || 403', () => {
    const status = 404;
    const statusText = 'Not Found';

    httpClient.get(url).subscribe({
      next: () => {
        fail('should have failed with the 404 error');
      },
      error: (err: HttpErrorResponse) => {
        expect(err.status).toEqual(status);
        expect(err.statusText).toEqual(statusText);
      },
    });

    const req = httpMock.expectOne(url);
    req.flush('404 Error', {
      status,
      statusText,
    });
  });

  it('should not call logout if error status is 401 and token is null', () => {
    const status = 401;
    const statusText = 'Unauthorized';
    const spy = spyOn(authService, 'logout');
    tokenSubject.next(null);

    httpClient.get(url).subscribe({
      next: () => {
        fail('should have failed with the 401 error');
      },
      error: (err: HttpErrorResponse) => {
        expect(err.status).toEqual(status);
        expect(err.statusText).toEqual(statusText);
      },
    });

    const req = httpMock.expectOne(url);
    req.flush('401 Error', {
      status,
      statusText,
    });

    expect(authService.tokenValue).toBeNull();
    expect(spy).not.toHaveBeenCalled();
  });

  it('should call logout if error status is 403 (or 401) and token exists', () => {
    const status = 403;
    const statusText = 'Access denied';
    const spy = spyOn(authService, 'logout');
    tokenSubject.next({ access_token: 'access' });

    httpClient.get(url).subscribe({
      next: () => {
        fail('should have failed with the 403 error');
      },
      error: (err: HttpErrorResponse) => {
        expect(err.status).toEqual(status);
        expect(err.statusText).toEqual(statusText);
      },
    });

    const req = httpMock.expectOne(url);
    req.flush('403 Error', {
      status,
      statusText,
    });

    expect(authService.tokenValue).not.toBeNull();
    expect(spy).toHaveBeenCalled();
  });
});
