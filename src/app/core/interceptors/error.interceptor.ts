import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, throwError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../services';
import { ApiError } from '@shared/models';

@Injectable({ providedIn: 'root' })
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<ApiError>> {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        if ([401, 403].includes(error.status) && this.authService.tokenValue) {
          // auto logout if 401 or 403 response returned from api
          this.authService.logout();
        }

        const err: ApiError = {
          status: error.status,
          statusText: error.statusText,
          message: error.error.message,
        };

        return throwError(() => err);
      })
    );
  }
}
