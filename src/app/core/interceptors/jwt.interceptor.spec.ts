import { BehaviorSubject } from 'rxjs';
import { AuthToken } from '@shared/models';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AuthService } from '@core/services';
import { TestBed } from '@angular/core/testing';
import { JwtInterceptor } from '@core/interceptors/jwt.interceptor';
import { environment } from '@env/environment';

const tokenSubject = new BehaviorSubject<AuthToken | null>(null);
const AuthServiceStub = {
  get tokenValue() {
    return tokenSubject.value;
  },
};

describe('JwtInterceptor', () => {
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
  let authService: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: JwtInterceptor,
          multi: true,
        },
        { provide: AuthService, useValue: AuthServiceStub },
      ],
      imports: [HttpClientTestingModule],
    });

    httpMock = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
    authService = TestBed.inject(AuthService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should add auth header if token exists and url is API url', () => {
    const url = `${environment.apiUrl}/test`;
    const token = 'access';
    tokenSubject.next({ access_token: token });

    httpClient.get(url).subscribe();
    const req = httpMock.expectOne(url);

    expect(req.request.headers.has('Authorization')).toBeTruthy();
    expect(req.request.headers.get('Authorization')).toEqual(`Bearer ${token}`);
    expect(req.request.headers.get('Content-Type')).toEqual('application/json');
  });

  it('should not add auth header if token is not exists', () => {
    const url = `${environment.apiUrl}/test`;
    tokenSubject.next(null);

    httpClient.get(url).subscribe();
    const req = httpMock.expectOne(url);

    expect(req.request.headers.has('Authorization')).toBeFalsy();
    expect(req.request.headers.get('Content-Type')).toEqual('application/json');
  });

  it('should not add auth header if url is not API url', () => {
    const url = 'https://test.com';
    const token = 'access';
    tokenSubject.next({ access_token: token });

    httpClient.get(url).subscribe();
    const req = httpMock.expectOne(url);

    expect(req.request.headers.has('Authorization')).toBeFalsy();
    expect(req.request.headers.get('Content-Type')).toEqual('application/json');
  });
});
