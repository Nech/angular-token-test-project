# GolangApi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.

## Project description

The application implements two authorization tokens. 
Access token refresh happens in silent mode, unnoticed by the user. 
When the refresh token expires, the user will be redirected to the login page.

## Backend API

Please, use [this project](https://gitlab.com/Nech/golang-token-api)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
